#!/usr/bin/perl

# Colors.cgi by Caspian (caspian@twu.net)
# Released under the GPL (use any version from 2 onwards), with no
# warranties expressed or implied.
#
# You can read the GPL online at http://www.gnu.org/copyleft/gpl.html

# --------------------------------------------------------------------------
# --------------------------------------------------------------------------
#
# PARAMETERS TO SET:
#
$defaultbackground = "#FFFFFF";		# The background the page opens with.
$colordb = "webcolors.txt";		# File containing the colors, CR-
					# delimited.
$imgdir = "/images";		# What to preface the img src= of the sample
			# images with. Can be a relative path like ".."
			# or something like "http://www.myhost.com/images/".

$showsampleimages = 1;	# If set to 1, displays the sample GIFs. If set to
                        # 0, does not display the sample GIFs. (Some
                        # admins might have no need or desire to see the
                        # sample GIFs.)

$swatchwidth = 75;	# Width of the color swatches.
$swatchborder = 1;	# Size of the black border around the swatches.
$rowsize = 8;		# How many swatches per row?

$extremethreshold = 200;	# Used in determining whether black or white
				# will be used in displaying a swatch.
				# If the sum of the values of R, G and B
				# exceed $totalthreshold (below) OR if
				# G (or R or B, if the appropriate
				# lines of code have been uncommented...
				# green tends to appear visually
				# "brighter" on most screens, and so this
				# is more important to look at for green)
				# exceeds $extremethreshold, the color
				# will be assumed too visually "bright"
				# for white to work right, so
				# black will be used. For instance, while
				# 00FF00 (pure, bright green) might not
				# trip the $totalthreshold check, it WILL
				# trip the $extremethreshold check, and
				# therefore black will be used.

$totalthreshold = 420;		# See above.
#
# --------------------------------------------------------------------------
# YOU SHOULD NOT NEED TO EDIT BELOW THIS LINE!
# --------------------------------------------------------------------------
# --------------------------------------------------------------------------

$version = "1.03";	# Version number
$basetitle = "Colors.cgi $version by Caspian";	# The base title.


sub nbspize {
   local ($instring) = $_[0];
   $instring =~ s/\ /\&nbsp\;/g;
   return $instring;
}

sub decimalize {
   local ($incolor) = $_[0];
   $incolorcopy = $incolor;	# We'll work on a copy.

   $bluehex2 = chop $incolorcopy;
   $bluehex1 = chop $incolorcopy;
   $bluehex = "${bluehex1}${bluehex2}";
   $bluecount = hex $bluehex; 

   $greenhex2 = chop $incolorcopy;
   $greenhex1 = chop $incolorcopy;
   $greenhex = "${greenhex1}${greenhex2}";
   $greencount = hex $greenhex; 

   $redhex2 = chop $incolorcopy;
   $redhex1 = chop $incolorcopy;
   $redhex = "${redhex1}${redhex2}";
   $redcount = hex $redhex; 

   $returnstring = "$redcount,&nbsp;$greencount,&nbsp;$bluecount";
   return $returnstring;
}

sub oppositize {
   local ($incolor) = $_[0];
   $incolorcopy = $incolor;	# We'll work on a copy.

   $bluehex2 = chop $incolorcopy;
   $bluehex1 = chop $incolorcopy;
   $bluehex = "${bluehex1}${bluehex2}";
   $bluecount = hex $bluehex; 

   $greenhex2 = chop $incolorcopy;
   $greenhex1 = chop $incolorcopy;
   $greenhex = "${greenhex1}${greenhex2}";
   $greencount = hex $greenhex; 

   $redhex2 = chop $incolorcopy;
   $redhex1 = chop $incolorcopy;
   $redhex = "${redhex1}${redhex2}";
   $redcount = hex $redhex; 

   $ubercount = $redcount + $bluecount + $greencount;
   $noextremes = 1;	# By default.
#   if ($redcount > $extremethreshold) { $noextremes = 0; }
   if ($greencount > $extremethreshold) { $noextremes = 0; }
#   if ($bluecount > $extremethreshold) { $noextremes = 0; }

   if (($ubercount < $totalthreshold) && ($noextremes == 1)) {
      $returnstring = "FFFFFF";
   } else {
      $returnstring = "000000";
   }
   return $returnstring;
}

sub complementize {
   local ($incolor) = $_[0];
   $incolorcopy = $incolor;	# We'll work on a copy.

   $bluehex2 = chop $incolorcopy;
   $bluehex1 = chop $incolorcopy;
   $bluehex = "${bluehex1}${bluehex2}";
   $bluecount = hex $bluehex; 

   $bluerevhex = sprintf "%.2lX", (255 - $bluecount);

   $greenhex2 = chop $incolorcopy;
   $greenhex1 = chop $incolorcopy;
   $greenhex = "${greenhex1}${greenhex2}";
   $greencount = hex $greenhex; 

   $greenrevhex = sprintf "%.2lX", (255 - $greencount);

   $redhex2 = chop $incolorcopy;
   $redhex1 = chop $incolorcopy;
   $redhex = "${redhex1}${redhex2}";
   $redcount = hex $redhex; 

   $redrevhex = sprintf "%.2lX", (255 - $redcount);


   $ubercount = $redcount + $bluecount + $greencount;
   $noextremes = 1;	# By default.
#   if ($redcount > $extremethreshold) { $noextremes = 0; }
   if ($greencount > $extremethreshold) { $noextremes = 0; }
#   if ($bluecount > $extremethreshold) { $noextremes = 0; }

   $returnstring = "${redrevhex}${greenrevhex}${bluerevhex}";
   return $returnstring;
}

sub percentize {
   local ($incolor) = $_[0];
   $incolorcopy = $incolor;	# We'll work on a copy.

   $bluehex2 = chop $incolorcopy;
   $bluehex1 = chop $incolorcopy;
   $bluehex = "${bluehex1}${bluehex2}";
   $bluecount = hex $bluehex; 

   $bluepercent = sprintf "%.2f", ($bluecount * 100 / 255);

   $greenhex2 = chop $incolorcopy;
   $greenhex1 = chop $incolorcopy;
   $greenhex = "${greenhex1}${greenhex2}";
   $greencount = hex $greenhex; 

   $greenpercent = sprintf "%.2f", ($greencount * 100 / 255);

   $redhex2 = chop $incolorcopy;
   $redhex1 = chop $incolorcopy;
   $redhex = "${redhex1}${redhex2}";
   $redcount = hex $redhex; 

   $redpercent = sprintf "%.2f", ($redcount * 100 / 255);


   $ubercount = $redcount + $bluecount + $greencount;
   $noextremes = 1;	# By default.
#   if ($redcount > $extremethreshold) { $noextremes = 0; }
   if ($greencount > $extremethreshold) { $noextremes = 0; }
#   if ($bluecount > $extremethreshold) { $noextremes = 0; }

   $returnstring = "${redpercent}%, ${greenpercent}%, ${bluepercent}%";
   return $returnstring;
}


sub ColMarkerRow {
local ($invalign) = $_[0];
print "<tr><td>&nbsp;</td>";
for ($tmpcount = 1; $tmpcount <= $rowsize; $tmpcount++) {
   print qq^
      <td valign="$invalign" align="center"><div align="center">
<font color="#$bgoppositized">$tmpcount</font>
      </div></td>
   ^;
}
print "</tr>";
}

sub RowLabel {
print qq^
<td valign="middle" align="right">
<font color="#$bgoppositized">$currrow</font></td>
^;
}

sub percentagestohex {
#   $redhex = sprintf "%.2lX", ($in{'redpercent'} * 255 / 100);
#   $greenhex = sprintf "%.2lX", ($in{'greenpercent'} * 255 / 100);
#   $bluehex = sprintf "%.2lX", ($in{'bluepercent'} * 255 / 100);

# This tends to undershoot things. For instance, "33.33", "33.33",
# "33.33" does NOT come out 555555. The following trick-- subtracting
# the numbers from 100, THEN converting to decimal, then subtracting
# that decimal number from 255 and converting to hex-- does the job nicer,
# and without a CPU-costly loop (e.g. counting upwards in hex until the
# number as a float >= the one we're looking for)...

$reddiff = sprintf "%.2d", ((100 - $in{'redpercent'}) * 255 / 100);
$greendiff = sprintf "%.2d", ((100 - $in{'greenpercent'}) * 255 / 100);
$bluediff = sprintf "%.2d", ((100 - $in{'bluepercent'}) * 255 / 100);

$rednum = 255 - $reddiff;
$greennum = 255 - $greendiff;
$bluenum = 255 - $bluediff;

$redhex = sprintf "%.2lX", $rednum;
$greenhex = sprintf "%.2lX", $greennum;
$bluehex = sprintf "%.2lX", $bluenum;

   $returnval = "${redhex}${greenhex}${bluehex}";
   return $returnval;
}

sub numberstohex {
   $redhex = sprintf "%.2lX", $in{'rednum'};
   $greenhex = sprintf "%.2lX", $in{'greennum'};
   $bluehex = sprintf "%.2lX", $in{'bluenum'};
   $returnval = "${redhex}${greenhex}${bluehex}";
   return $returnval;
}


# Ubermain

print "Content-type: text/html\n\n";

$cginame = "$ENV{'REQUEST_URI'}";    # The name of this CGI.
if (!($cginame =~ /\S/)) {
   # Roxen Challenger, and perhaps other Web servers as well, uses the
   # variable "SCRIPT_NAME" instead of the variable "REQUEST_URI".
   # Thanks to Rodolfo Pilas for reporting this!
   $cginame = $ENV{'SCRIPT_NAME'};
}

@sillyarray = split /\?/,$cginame;
$cginame = $sillyarray[0];


$subtitle = "";			# By default, no subtitle.
$bgcolor = "$defaultbackground";	# Sets BGcolor to default value.

use CGI qw(:cgi-lib);	# Thank you to the author of this indispensable
			# library!

&ReadParse;		# Populate the $IN hash with the HTTP variables.

if (($in{'redpercent'} =~ /\S/) && ($in{'greenpercent'} =~ /\S/) && ($in{'bluepercent'} =~ /\S/)) {
   $bgcolor = percentagestohex($in{'redpercent'}, $in{'greenpercent'}, $in{'bluepercent'});
} else {
   if (($in{'rednum'} =~ /\S/) && ($in{'greennum'} =~ /\S/) && ($in{'bluenum'} =~ /\S/)) {
      $bgcolor = numberstohex($in{'rednum'}, $in{'greennum'}, $in{'bluenum'});
   } else {
      if ($in{'bgcolor'} =~ /\S/) {
         $bgcolor = $in{'bgcolor'};
      } else {
         $bgcolor = "FFFFFF";
      }
   }
}

if ($in{'rowsize'} =~ /\S/) {
   if (($in{'rowsize'} > 0) && ($in{'rowsize'} < 100)) { # Paranoid sanity
							 # checks.
      $rowsize = $in{'rowsize'};
   }
}

print qq^<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
      "http://www.w3.org/TR/REC-html40/loose.dtd">

<html>
<head><title>${basetitle}${subtitle}</title>
<STYLE TYPE="text/css">
<!--A:link, A:visited, A:active { text-decoration: none }A:hover {
text-decoration: none; }--></STYLE>
</head>
<body bgcolor="$bgcolor">
^;

$oppositized = oppositize($bgcolor);
$bgoppositized = $oppositized;	# This one never gets changed, and so can
				# be safely used anywhere below this point.

print "<table width=\"100%\" border=0 cellspacing=0 cellpadding=0><tr><td align=left>";
print "<font size='+2' face='Arial Narrow, Helvetica, Sans Serif, Sans-Serif' color='$oppositized'>";
print "Now&nbsp;viewing:&nbsp;";



if (!($bgcolor =~ /\S/)) {
   print "No&nbsp;color!";
} else {
   $decimalized = nbspize(decimalize($bgcolor));
   print "#$bgcolor&nbsp;($decimalized)";
   $percentized = nbspize(percentize($bgcolor));
   print qq^
<font size="+1"><br>
(Decimal: $percentized)
</font>^;
}

print "</font></td>";
print qq^
<td width="100%" align="right" valign="top">
<form action="$cginame" method="post">
<input type="hidden" name="rowsize" value="$rowsize">
<table border=4 cellspacing=2 cellpadding=2>
<tr><td valign="top">
<table border=0 cellspacing=0 cellpadding=0>
<tr>
<td valign="top">
<font face="Arial, Helvetica, Sans-serif, Sans Serif" color="#$oppositized">
Color:
</font>
</td>
<td valign="top">
<div align="center">
<font color="#$oppositized">
#<input type="text" size="6" maxlength="6" name="bgcolor">
HEX
<br>
<b>-OR-</b>
<br>
<input type="text" size="5" maxlength="5" name="redpercent">% R, 
<input type="text" size="5" maxlength="5" name="greenpercent">% G,
<input type="text" size="5" maxlength="5" name="bluepercent">% B
<br>
<b>-OR-</b>
<br>
<input type="text" size="3" maxlength="3" name="rednum"> /255, 
<input type="text" size="3" maxlength="3" name="greennum"> /255,
<input type="text" size="3" maxlength="3" name="bluenum"> /255
<br>
</font>
</div>
</td>
<td valign="bottom">
<font face="Arial, Helvetica, Sans-serif, Sans Serif" color="#$oppositized">
<input type="submit" value="Change"></font>
</td>
</tr>
</table>
</td></tr></table>
</form>
<form action="$cginame" method="post">
<input type="hidden" name="bgcolor" value="$bgcolor">
<font face="Arial, Helvetica, Sans-serif, Sans Serif" color="#$oppositized">
Cells/row: </font>
<input type="text" size="2" maxlength="2" name="rowsize">
<font face="Arial, Helvetica, Sans-serif, Sans Serif" color="#$oppositized">
<input type="submit" value="Change"></font>
</form>

</td></tr></table><hr><br>\n\n
^;

open(READCOLORS, $colordb);
$printedcount = 0;	# This counter is used to determine the end of
			# rows.
print "<table border=0 cellspacing=4 cellpadding=$swatchborder>";

ColMarkerRow("bottom");

$currrow = 0;
$currcol = 1;

while (<READCOLORS>) {
   $_ =~ s/^[\s\#]*//;		# Eliminate leading whitespace and #s.
   $_ =~ s/[\s]*$//;		# Eliminate trailing whitespace.
   if (length($_) == 6) {	# Good, we can go on...
$inlist{$_} = 1;	# Mark this value down as being on the list.
      if (($printedcount % $rowsize) == 0) {
         $currrow++;
         $currcol = 1;
         print "<tr>";
         RowLabel();
      }
      $rowshash{$_} = $currrow;
      $colshash{$_} = $currcol;
      $decimalized = nbspize(decimalize($_));
      $theopposite = oppositize($_);
      print qq^<td valign="middle" align="center" bgcolor="#$bgoppositized">
<table border=0 cellspacing=0 cellpadding=3 width="100%"><tr>
<td valign="middle" align="center" bgcolor="#$_" width="100%">
<div align="center">
<a href="$cginame?rowsize=$rowsize&amp;bgcolor=$_">
<font color="#$theopposite" size="+1">
$_</font></a><br>
<a href="$cginame?rowsize=$rowsize&amp;bgcolor=$_">
<font color="#$theopposite" size="-1">$decimalized</font></a>
</div></td>
</tr></table></td>
^;
      $printedcount++;
      if (($printedcount % $rowsize) == 0) {
         print "</tr>\n\n";
      }
   }
   $currcol++;
}

$leftovers = $rowsize - ($printedcount % $rowsize);

if ($leftovers == $rowsize) { $leftovers = 0; }

if ($leftovers > 0) {
   print "<td colspan=\"$leftovers\"><br></td></tr>";
}

$oppositized = oppositize($bgcolor);

$complementized = complementize($bgcolor);
$complementizeddecimal = decimalize($complementized);

$complementoppositized = oppositize($complementized);

ColMarkerRow("top");

print qq^
</table>

<table width="100%" border=0 cellspacing=4 cellpadding=0>
^;

if ($showsampleimages == 1) {	# IF DESIRED, DISPLAY SAMPLE IMAGES.

if (!($imgdir =~ /\/$/)) {
   $imgdir .= '/';	# Make SURE $imgdir has the necessary trailing slash.
}

print qq^
<tr><td width=134 height=134><img src="${imgdir}whitewww.gif" width=134 height=134 alt=""></td>
<td><font size="+1" color="#$oppositized">
Image antialiased to white, WWW palette.
</font>
</td></tr>

<tr><td width=134 height=134><img src="${imgdir}whiteopt.gif" width=134 height=134 alt=""></td>
<td><font size="+1" color="#$oppositized">
Image antialiased to white, optimal palette.
</font>
</td></tr>

<tr><td width=134 height=134><img src="${imgdir}blackwww.gif" width=134 height=134 alt=""></td>
<td><font size="+1" color="#$oppositized">
Image antialiased to black, WWW palette.
</font>
</td></tr>

<tr><td width=134 height=134><img src="${imgdir}blackopt.gif" width=134 height=134 alt=""></td>
<td><font size="+1" color="#$oppositized">
Image antialiased to black, optimal palette.
</font>
</td></tr>

<tr><td width=134 height=134><img src="${imgdir}greywww.gif" width=134 height=134 alt=""></td>
<td><font size="+1" color="#$oppositized">
Image antialiased to 50% grey, WWW palette.
</font>
</td></tr>

<tr><td width=134 height=134><img src="${imgdir}greyopt.gif" width=134 height=134 alt=""></td>
<td><font size="+1" color="#$oppositized">
Image antialiased to 50% grey, optimal palette.
</font>
</td></tr>
^;
}	# IF DESIRED, DISPLAY SAMPLE IMAGES.

print qq^
<tr><td colspan="2">

<br><br>

&nbsp;&nbsp;&nbsp;
<font color="#$oppositized">White sample text:</font>
<font color="#FFFFFF">
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
</font>
<br><br>

&nbsp;&nbsp;&nbsp;
<font color="#$oppositized">Black sample text:</font>
<font color="#000000">
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
</font>
<br><br>

&nbsp;&nbsp;&nbsp;
<font color="#$oppositized">50% grey sample text:</font>
<font color="#7F7F7F">
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
</font>
<br><br>

&nbsp;&nbsp;&nbsp;
<font color="#$oppositized">Color wheel opposite is #$complementized 
($complementizeddecimal). Sample text:</font>
<font color="#$complementized">
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
</font>

<br><br>
<table border=0 cellspacing=0 cellpadding=5 bgcolor="#$complementized">
<tr><td>
<font color="#$complementoppositized">
Here is a sample swatch of the complementary color. This color 
^;

if ($inlist{$complementized} == 1) {
   print " <b>IS</b>";
} else {
   print " <b>IS NOT</b>";
}

print " on this color list.";

if ($inlist{$complementized} == 1) {
   $therow = $rowshash{$complementized};
   $thecol = $colshash{$complementized};
   print qq^
 (swatch $thecol in row $therow, or 
 <a href="$cginame?rowsize=$rowsize&amp;bgcolor=$complementized"><u>
<font color="#$complementoppositized">
follow this link</font></u></a>)
^;
}

print qq^
<br><br>
&nbsp;&nbsp;&nbsp;Here is how text of the color you're viewing looks
against its complement:
<br><br>
</font>
<font color="#$bgcolor">
&nbsp;&nbsp;&nbsp;
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
The quick brown fox jumped over the lazy sleeping dog.
</font>
<br><br><br><br>
</td></tr></table>



</td></tr>


</table>

</body>
</html>
^;
