# colors-cgi in a docker

An old-style web application tool for choosing colors from a customizable palette. Original programed by Jon Blank <caspian@twu.net> and distributed with the General Public License (GPL).

The `colors-cgi-1.03/` folder has the version 1.03 of July 25, 2000 as it had been shared by the author. 

This repository is the *dockerization* of that application.

**DEMO** is located [here](https://colors.w.uy/cgi-bin/colors.cgi) 

![screen capture](screencap-colors-cgi.png)


## Build

Always under your own risk, this is a `docker build .` repository.

## Setup

The Dokerfile FROM takes the `nginx` official docker image. These are the build and run commands:

```
docker build -t p/colors .
docker run --rm -p 8080:80 -d --name colors-cgi p/colors
wget http://localhost:8080/cgi-bin/colors.cgi
docker stop colors-cgi
```

## Thanks to

Jon Blank <caspian@twu.net> for the great colors-cgi Perl program.

## License

colors-cgi progmam is here as-is, without modification, and is licensed under GPL. 

The other files of my authorship are licensed under [WTFPL version 2](http://www.wtfpl.net/)

![](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png)
