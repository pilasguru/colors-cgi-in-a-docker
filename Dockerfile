FROM nginx:1.24.0-bullseye
LABEL maintainer="pilasguru@gmail.com" \
      version="2.0" \
      description="Image to run colors-cgi v1.03 of Jon Blank caspian@twu.net"

RUN apt-get clean && apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y spawn-fcgi fcgiwrap liberror-perl liburi-perl libcgi-pm-perl && \
    apt-get clean

COPY nginx/default.conf /etc/nginx/conf.d/default.conf
COPY colors-cgi-1.03/colors.cgi colors-cgi-1.03/webcolors.txt /usr/share/nginx/cgi-bin/
COPY colors-cgi-1.03/*.gif /usr/share/nginx/html/images/


RUN sed -i 's/www-data/nginx/g' /etc/init.d/fcgiwrap; \
    chown nginx:nginx /etc/init.d/fcgiwrap; \
    chmod +x /usr/share/nginx/cgi-bin/*

EXPOSE 80

WORKDIR /var/www
CMD ["/bin/sh", "-c", "/etc/init.d/fcgiwrap start && nginx -g 'daemon off;'"]

